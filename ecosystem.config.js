module.exports = {
    apps: [
        {
            name: 'ND Growth Nuxt',
            script: './node_modules/nuxt/bin/nuxt.js',
            args: 'start'
        }
    ]
}
