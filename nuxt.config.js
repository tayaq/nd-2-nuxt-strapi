export default {
    ssr: true,
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'ND Group',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
        ],
        script: [
            {
                src: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.3/gsap.min.js',
                defer: true,
            },
            {
                src: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.10.3/ScrollTrigger.min.js',
                defer: true,
            },
            {
                src: '/js/src/split.js',
                defer: true,
            },
            {
                src: '/js/src/index.js',
                defer: true,
            }
        ],
        bodyAttrs: {
            class: 'page'
        }
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '@/assets/css/style.min.css'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '~/plugins/gtm'
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: ['@nuxtjs/dotenv'],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/strapi',
        '@nuxtjs/axios',
        '@nuxtjs/gtm',
        'cookie-universal-nuxt'
    ],

    gtm: {
        enabled: true,
        autoInit: false,
        // debug: true,
        // pageTracking: true,
    },

    strapi: {
        url: process.env.STRAPI_URL || `http://localhost:1402/api`,
        entities: []
    },

    axios: {
        baseURL: process.env.STRAPI_URL || `http://localhost:1401/api`,
    },

    publicRuntimeConfig: {
        axios: {
            browserBaseURL: process.env.BROWSER_BASE_URL
        }
    },

    privateRuntimeConfig: {
        axios: {
            baseURL: process.env.BASE_URL
        }
    },

    server: {
        port: '3000'
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
}
