function NDXScroll() {
    if (scrollY <= 5 && document.querySelector('.header').classList.contains('header--forwards')) {
        document.querySelector('.header').classList.add('header--animation-reverse');
        page.classList.add('page--disabled');

        setTimeout(() => {
            window.addEventListener('scroll', headerScrollEventNDX);
            document.querySelector('.header').classList.remove('header--forwards');
            document.querySelector('.header').classList.remove('header--animation-reverse');
        }, 3500)

    }
}

function headerScrollEventNDX() {
    document.querySelector('.header').classList.add('header--animation');
    setTimeout(() => {
        window.removeEventListener('scroll', headerScrollEventNDX);
        page.classList.remove('page--disabled');
        window.scrollTo({
            top: document.querySelector('.header').offsetHeight,
            behavior: "smooth"
        });

        setTimeout(() => {
            document.querySelector('.header').classList.remove('header--animation');
            document.querySelector('.header').classList.add('header--forwards');
        }, 1000)

    }, 3500)

    window.addEventListener('scroll', NDXScroll);

}
