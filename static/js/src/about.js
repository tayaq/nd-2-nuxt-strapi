if (document.body.classList.contains('page--about')) {

    const animationItems = document.querySelectorAll('.about__p, .about__photo, .about__name, .about__post, .about__quote, .about__preview, .about__play, .journey__desc, .journey__content, .journey__image, .journey__year');

    for (let item of animationItems) {
        gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
            duration: .5,
            autoAlpha: 1,
            y: 0,
            scrollTrigger: {
                trigger: item,
                start: 'top 90%',
                once: true
            }
        });
    }

    let tlCursor = gsap.timeline({ paused: true });

    tlCursor.to('.x-cursor', {
        opacity: 0,
        ease: 'none',
        display: 'none',
        duration: .1
    })


    let iframe = document.querySelector('.about__iframe');
    let player = $f(iframe);

    const video = document.querySelector('.about__open-video');

    iframe.addEventListener('mouseenter', () => {
        tlCursor.play();
    });

    iframe.addEventListener('mouseleave', () => {
        tlCursor.reverse();
    });

    video.addEventListener('click', () => {
        document.querySelector('.about__video').classList.add('about__video--open');
        player.api('play');
    })


    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('about__video--open')) {
            e.target.classList.remove('about__video--open');
            player.api('pause');
        }
    });

}
