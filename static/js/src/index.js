gsap.registerPlugin(SplitText, ScrollTrigger);

const page = document.querySelector('.page');

const appHeight = () => {
    const doc = document.documentElement
    doc.style.setProperty('--app-height', `${window.innerHeight}px`)
}
window.addEventListener('resize', appHeight)
appHeight();

// function messageSplits() {
//     gsap.utils.toArray('[data-gsap="split-message"]').forEach(el => {
//         gsap.from(SplitText.create(el, { type: "words, chars" }).chars, {
//             opacity: 0,
//             y: '15%',
//             transformOrigin: "100% 50%",
//             stagger: 0.1,
//             scrollTrigger: {
//                 trigger: el,
//                 start: "top 90%",
//                 once: true
//             }
//         });
//     });
// }
//
// messageSplits();

const footerItems = document.querySelectorAll('.footer__logo, .footer__item, .footer__address, .footer__contacts, .footer__info, .footer__linkedin, .footer__copyright, .footer__created');

for (let item of footerItems) {
    gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
        duration: .5,
        autoAlpha: 1,
        y: 0,
        scrollTrigger: {
            trigger: item,
            start: 'top 105%',
            once: true
        }
    });
}

if (document.body.classList.contains('page--main') || document.body.classList.contains('page--about')) {

}

// if (!document.body.classList.contains('page--main')) {

// }

let resizeTimer;
window.addEventListener("resize", () => {
    document.body.classList.add('page--resize');
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => {
        document.body.classList.remove('page--resize');
    }, 400);
});

window.addEventListener('scroll', (e) => {
    if (scrollY > 150) page.classList.add('page--menu');
    else page.classList.remove('page--menu');
});


var cursorDot = {
    delay: 8,
    _x: 0,
    _y: 0,
    endX: (window.innerWidth / 2),
    endY: (window.innerHeight / 2),
    cursorVisible: true,
    cursorEnlarged: false,
    $xCursor: document.querySelector('.x-cursor'),
    $dot: document.querySelector('.cursor-dot'),
    $cursorCircle: document.querySelector('.cursor-findout'),

    init: function () {
        this.dotSize = this.$dot.offsetWidth;

        this.setupEventListeners();
    },

    setupEventListeners: function () {
        var self = this;

        document.querySelectorAll('a').forEach(function (el) {
            el.addEventListener('mouseover', function () {
                self.cursorEnlarged = true;
                self.toggleCursorSize();
            });
            el.addEventListener('mouseout', function () {
                self.cursorEnlarged = false;
                self.toggleCursorSize();
            });
        });

        document.querySelectorAll('.cases__item, .partners__item:not(.partners__item--more), .short-news__item').forEach(function (el) {
            el.addEventListener('mouseover', function () {
                self.cursorEnlarged = true;
                self.toggleCursorSize();
                self.$xCursor.classList.add('x-cursor-active');
            });
            el.addEventListener('mouseout', function () {
                self.cursorEnlarged = false;
                self.toggleCursorSize();
                self.$xCursor.classList.remove('x-cursor-active')
            });
        });

        document.addEventListener('mousedown', function () {
            self.cursorEnlarged = true;
            self.toggleCursorSize();
        });
        document.addEventListener('mouseup', function () {
            self.cursorEnlarged = false;
            self.toggleCursorSize();
        });


        document.addEventListener('mousemove', function (e) {
            self.cursorVisible = true;
            self.toggleCursorVisibility();

            self.endX = e.clientX;
            self.endY = e.clientY;
            self.$xCursor.style.top = self.endY + 'px';
            self.$xCursor.style.left = self.endX + 'px';
        });

        document.addEventListener('mouseenter', function (e) {
            self.cursorVisible = true;
            self.toggleCursorVisibility();
            self.$dot.style.opacity = 1;
        });

        document.addEventListener('mouseleave', function (e) {
            self.cursorVisible = true;
            self.toggleCursorVisibility();
            self.$dot.style.opacity = 0;
        });
    },


    toggleCursorSize: function () {
        var self = this;

        if (self.cursorEnlarged) {
            self.$dot.style.transform = "translate3d(-50%, -50%, 0) scale(1.8)";
        } else {
            self.$dot.style.transform = 'translate3d(-50%, -50%, 0) scale(1)';
        }
    },

    toggleCursorVisibility: function () {
        var self = this;

        if (self.cursorVisible) {
            self.$dot.style.opacity = 1;
        } else {
            self.$dot.style.opacity = 0;
        }
    }
}
cursorDot.init();
