const companiesSelect = document.querySelectorAll('.companies__select');

for (let select of companiesSelect) {
    select.addEventListener('click', () => {
        select.classList.toggle('companies__select--active');
    });
}

const companiesAnchors = document.querySelectorAll('.companies__anchor');

for (let anchor of companiesAnchors) {
    anchor.addEventListener('click', (e) => {
        addClassAnchor(anchor.getAttribute('data-slide'));
        swiper.slideTo(anchor.getAttribute('data-slide'));
    });
}

function addClassAnchor(index) {
    let activeAnchors = document.querySelectorAll('.companies__anchor--active');
    if (activeAnchors) {
        for (let active of activeAnchors) {
            active.classList.remove('companies__anchor--active');
        }
    }
    let anchors = document.querySelectorAll(`[data-slide="${index}"]`);
    for (let anchor of anchors) {
        anchor.classList.add('companies__anchor--active');
    }

}

const animationItems = document.querySelectorAll('.portfolio-message__content, .approach__title, .approach__label, .approach__item, .companies__label');

for (let item of animationItems) {
    gsap.fromTo(item, { autoAlpha: 0, y: '15%' }, {
        delay: item.dataset.delay || 0,
        duration: .5,
        autoAlpha: 1,
        y: 0,
        scrollTrigger: {
            trigger: item,
            start: 'top 90%',
            once: true
        }
    });
}

let swiper = new Swiper('.swiper-container', {
    direction: 'vertical',
    sliderPerView: 1,
    spaceBetween: 0,
    initialSlide: 0,
    mousewheel: true,
    speed: 1000,
    navigation: {
        nextEl: '.companies__button--down',
        prevEl: '.companies__button--up',
    },
    slideActiveClass: 'companies__item--active',
    on: {
        slideChange: (e) => {
            companiesBlock.setAttribute('data-companies', e.activeIndex);
            addClassAnchor(e.activeIndex);
        },
        slideChangeTransitionStart: () => {
            window.scrollTo({
                top: getElementOffset(companiesBlock).top,
                behavior: 'smooth'
            });
        },
        touchStart: () => {
            onSlider = true;
        },
        touchEnd: (e) => {
            if (e.isEnd && e.swipeDirection === 'next') {
                window.scrollTo({
                    top: getElementOffset(footerBlock).top,
                    behavior: 'smooth'
                });
                setTimeout(() => {
                    onSlider = false;
                }, 1500)
            }
            if (e.isBeginning && e.swipeDirection === 'prev') {
                window.scrollTo({
                    top: getElementOffset(companiesBlock).top - window.innerHeight,
                    behavior: 'smooth'
                });
                setTimeout(() => {
                    onSlider = false;
                }, 1500)
            }
        },
        slideChangeTransitionEnd: (e) => {
            lastSlide = e.activeIndex === swiper.slides.length - 1;
            firstSlide = e.activeIndex === 0;
        }
    }
})

let firstSlide = true;
let lastSlide = false;
let canBeScrolled = true;
let scrollDirection;
let prevScrollPos = 0;
let companiesBlock = document.querySelector('.companies__container');
let footerBlock = document.querySelector('.footer');

if (location.hash) {
    let index = location.hash.replace('#', '');
    window.scrollTo({
        top: companiesBlock.offsetTop,
        behavior: 'smooth'
    });
    swiper.slideTo(index - 1);
    addClassAnchor(index - 1);
} else addClassAnchor(0);

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    let lastY;

    let onSlider = false;
    let scrollTimeoutMobile;

    window.addEventListener('scroll', (e) => {
        if (canBeScrolled) {
            if ((scrollY + 20 >= getElementOffset(companiesBlock).top - window.innerHeight)
                && !onSlider
                && !lastSlide
                && scrollY > lastY || scrollY < lastY && !onSlider && lastSlide) {
                canBeScrolled = false;
                window.scrollTo({
                    top: getElementOffset(companiesBlock).top,
                    behavior: 'smooth'
                });
            } else if (
                scrollY + window.innerHeight >= getElementOffset(footerBlock).top
                && !onSlider
                && scrollY > lastY
                && lastSlide) {
                canBeScrolled = false;
                window.scrollTo({
                    top: getElementOffset(footerBlock).top,
                    behavior: 'smooth'
                });
            }
            console.log(scrollY + window.innerHeight, getElementOffset(footerBlock), onSlider);
            clearTimeout(scrollTimeoutMobile);
            scrollTimeoutMobile = setTimeout(() => {
                canBeScrolled = true;
            }, 500)
        }
        lastY = scrollY <= 0 ? 0 : scrollY;
    })

}

window.addEventListener('wheel', () => {
    if ((scrollY + window.innerHeight > getElementOffset(companiesBlock).top)) {
        scroll(getElementOffset(companiesBlock).top);
    }
});

companiesBlock.addEventListener('wheel', (e) => {
    if (e.deltaY > 0 && swiper.activeIndex === swiper.slides.length - 1) {
        if (lastSlide) scroll(getElementOffset(footerBlock).top);
    } else if (e.deltaY < 0 && swiper.activeIndex === swiper.slides.length - 1) {
        scroll(getElementOffset(companiesBlock).top);
    } else if (e.deltaY < 0 && swiper.activeIndex === 0) {
        if (firstSlide) scroll(getElementOffset(companiesBlock).top - window.innerHeight);
    }
});

function getElementOffset(el) {
    const rect = el.getBoundingClientRect();

    return {
        top: rect.top + window.scrollY,
        left: rect.left + window.scrollX,
    };
}

function scroll(top) {
    if (canBeScrolled) {
        canBeScrolled = false;
        window.scrollTo({
            top: top,
            behavior: 'smooth'
        });
        setTimeout(() => {
            canBeScrolled = true;
        }, 1000)
    }
}

let scrollTimeout;

function scrollMobile(block) {
    clearTimeout(scrollTimeout);
    scrollTimeout = setTimeout(() => {
        block.scrollIntoView({
            behavior: 'smooth'
        });
    }, 100);
}

