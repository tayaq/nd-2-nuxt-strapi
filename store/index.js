export const state = () => ({
    headers: [],
    setting: {}
})

export const mutations = {
    setHeaders(state, headers) {
        state.headers = headers;
    },
    setSetting(state, setting) {
        state.setting = setting;
    },
}

export const actions = {

    async fetchHeaders({ commit }) {
        const response = await this.$strapi.find('headers', {
            populate: ['background', 'seo']
        })
        await commit('setHeaders', response.data);
    },

    async fetchSetting({ commit }) {
        const response = await this.$strapi.find('setting', {
            populate: ['contacts', 'og', 'og.image', 'favicon', 'faviconDark']
        })
        await commit('setSetting', response.data);
    },

    async nuxtServerInit({ dispatch }) {
        await dispatch('fetchHeaders');
        await dispatch('fetchSetting');
    }
}


export const getters = {
    headers: state => state.headers,
    setting: state => state.setting,
}
