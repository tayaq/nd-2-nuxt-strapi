module.exports = ({ env }) => ({
  'transformer': {
    enabled: true,
    config: {
      prefix: '/api/',
      responseTransforms: {
        removeAttributesKey: true,
        removeDataKey: true,
      }
    }
  },
  'preview-button': {
    config: {
      contentTypes: [
        {
          uid: 'api::post.post',
          targetField: 'slug',
          published: {
            basePath: 'news',
          },
        },
      ],
    },
  },
  'import-export-entries': {
    enabled: false,
  },
  seo: {
    enabled: true,
  },
  'migrations': {
    enabled: false,
    config: {
      autoStart: true,
      migrationFolderPath: 'migrations'
    },
  }
});
