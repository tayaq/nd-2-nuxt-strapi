module.exports = {
  apps: [
    {
      name: 'ND Growth Strapi',
      script: 'npm',
      args: 'start',
    },
  ],
};
